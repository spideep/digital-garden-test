## Installation

 - Clone the repository.
 - `cd` to drupal8-d4d folder.
 - Run `make u`
 - Go to php container bash : Inside drupal8-d4d run `docker-compose exec php bash`
 - Run `composer update` in order to require dependencies (like bootstrap or ds)
 - Inside bash `cd` 'web' folder
 - Run `drush si --y`
 - This will install a standard profile.
 - Import the database located in `codebase/db-set16.sql`

## Compile scss

If you need to compile scss to css in the dg_theme, please follow these steps:

 - `cd codebase/web/themes/custom/dg_theme`
 - `compass compile --no-sourcemap --no-line-comments --output-style expanded --e production`

## Import configs
Since you import the database there is no need to import them. But just in case this is the command to do it.

 - Go to php container bash : Inside drupal8-d4d run `docker-compose exec php bash`
 - Inside bash `cd` 'web' folder
 - Run `drush cim --y`
